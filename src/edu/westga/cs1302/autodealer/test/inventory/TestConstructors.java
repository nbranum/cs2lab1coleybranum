package edu.westga.cs1302.autodealer.test.inventory;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * Ensures correct functionality of the Automobile constructor.
 * 
 * @author nbranum1
 *
 */
public class TestConstructors {

	
	
	@Test 
	void testValidConstructor() {
		Inventory one= new Inventory("Marshalls");
		assertAll(() -> assertEquals("Marshalls", one.getDealershipName()));
	
	}
	
	@Test
	void testValidConstructorBoth() {
		ArrayList<Automobile> cars =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars);
		Automobile FF=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
	    cars.add(FF);
	    
	    assertAll(() -> assertEquals("Marshalls",two.getDealershipName()),
	    		() -> assertEquals(cars.size(), two.numAutomobiles()));
	}
	
	@Test
	void testNulldealershipName(){
		assertThrows(IllegalArgumentException.class, () -> new Inventory(null));
	}
	
	@Test
	void testEmptydealershipName() {
		assertThrows(IllegalArgumentException.class, () -> new Inventory(""));
	}
	
	@Test
	void testAddAutomobile() {
		Automobile FF=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		ArrayList<Automobile> cars =new ArrayList<Automobile>();
	    Inventory two=new Inventory("Marshalls",cars);
	    assertEquals(true, two.addAutomobile(FF));
	}
	
	@Test 
	void testGetOldestYear1() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
	    Inventory two=new Inventory("Marshalls",cars2);
		//empty test
	    assertThrows(IllegalArgumentException.class, () -> two.getOldestYear());
	}
	
	@Test 
	void testGetOldestYear2() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFo);
	    cars2.add(FFn);
	    cars2.add(FFm);
	    Inventory two=new Inventory("Marshalls",cars2);
		//oldest first test
	    assertEquals(1960,two.getOldestYear());
	}
	
	@Test 
	void testGetOldestYear3() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFn);
	    cars2.add(FFo);
	    cars2.add(FFm);
	    Inventory two=new Inventory("Marshalls",cars2);
		//oldest middle test
	    assertEquals(1960,two.getOldestYear());
	}
	
	@Test 
	void testGetOldestYear4() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
	    Inventory two=new Inventory("Marshalls",cars2);
		//oldest last test
	    assertEquals(1960,two.getOldestYear());
	}
	
	@Test 
	void testGetOldestYear5() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
	    cars2.add(FFm);
	    Inventory two=new Inventory("Marshalls",cars2);
		//single element
	    assertEquals(2008,two.getOldestYear());
	}
	
	@Test 
	void testGetNewestYear1() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
	    Inventory two=new Inventory("Marshalls",cars2);
		//empty test
	    assertThrows(IllegalArgumentException.class, () -> two.getNewestYear());
	}
	
	@Test 
	void testGetNewestYear2() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFo);
	    cars2.add(FFm);
	    cars2.add(FFn);
	    Inventory two=new Inventory("Marshalls",cars2);
		//Newest last test
	    assertEquals(2010,two.getNewestYear());
	}
	
	@Test 
	void testGetNewestYear3() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFn);
	    cars2.add(FFo);
	    cars2.add(FFm);
	    Inventory two=new Inventory("Marshalls",cars2);
		//Newest first test
	    assertEquals(2010,two.getNewestYear());
	}
	
	@Test 
	void testGetNewestYear4() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
	    Inventory two=new Inventory("Marshalls",cars2);
		//Newest middle test
	    assertEquals(2010,two.getNewestYear());
	}
	
	@Test 
	void testGetNewestYear5() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
	    cars2.add(FFm);
	    Inventory two=new Inventory("Marshalls",cars2);
		//single element
	    assertEquals(2008,two.getNewestYear());
	}
	
	@Test 
	void testGetNumInRange1() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
		//all in range test
	    assertEquals(3,two.getNumInRange(1900, 2090));
	}
	
	@Test 
	void testGetNumInRange2() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
		//2 in range test
	    assertEquals(2,two.getNumInRange(2000, 2090));
	}
	
	@Test 
	void testGetNumInRange3() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
		//one in range test
	    assertEquals(1,two.getNumInRange(2009, 2090));
	}
	
	@Test 
	void testGetNumInRange4() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
		//none in range test
	    assertEquals(0,two.getNumInRange(2020, 2090));
	}
	
	@Test 
	void testGetNumInRange6() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
		//equal to lower bound 
	    assertEquals(3,two.getNumInRange(1960, 2090));
	}
	
	@Test 
	void testGetNumInRange7() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
		Automobile FFm=new Automobile("Ford", "Focus", 2008, 108132, 6900.99);
		Automobile FFn=new Automobile("Ford", "Focus", 2010, 108132, 6900.99);
		cars2.add(FFm);
	    cars2.add(FFn);
	    cars2.add(FFo);
		//equal to upper bound
	    assertEquals(3,two.getNumInRange(1900, 2010));
	}
	
	@Test 
	void testGetNumInRange8() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
	    cars2.add(FFo);
		//one element
	    assertEquals(1,two.getNumInRange(1900, 2090));
	}
	
	@Test 
	void testGetNumInRange9() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		//no elements
	    assertThrows(IllegalArgumentException.class, () -> two.getNumInRange(1900,2000));
	}
	
	@Test
	void testToString() {
		ArrayList<Automobile> cars2 =new ArrayList<Automobile>();
		Inventory two=new Inventory("Marshalls",cars2);
		Automobile FFo=new Automobile("Ford", "Focus", 1960, 108132, 6900.99);
	    cars2.add(FFo);
	    assertEquals("Dealership: Marshalls #Autos: 1",two.toString());
	}
}
