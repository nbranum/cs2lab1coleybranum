package edu.westga.cs1302.autodealer.resources;

/**
 * Contains exception messages for the user.
 * 
 * @author CS1302
 *
 */
public class UI {

	public static final String INVALID_PRICE = "price must be >= 0.";
	public static final String INVALID_MILES = "miles must be >= 0.";
	public static final String INVALID_YEAR = "year must be >= 1885.";
	public static final String MODEL_CANNOT_BE_EMPTY = "model cannot be empty.";
	public static final String MODEL_CANNOT_BE_NULL = "model cannot be null.";
	public static final String MAKE_CANNOT_BE_EMPTY = "make cannot be empty.";
	public static final String MAKE_CANNOT_BE_NULL = "make cannot be null.";
	public static final String DSHIPNAME_CANNOT_BE_NULL = "dealership name cannot be null";
	public static final String DSHIPNAME_CANNOT_BE_EMPTY = "dealership name cannot be empty";
	public static final String COLLECTION_VALUE_INVALID = "collection size invalid";
}
