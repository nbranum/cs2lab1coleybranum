package edu.westga.cs1302.autodealer.model;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The class Automobile.
 * 
 * @author nbranum1
 * @version 1
 */
public class Automobile {

	public static final int MILES_LOWER_BOUND = 0;
	public static final double PRICE_LOWER_BOUND = 0.0;
	public static final int LOWER_BOUND_YEAR = 1885;

	private String make;
	private String model;
	private int year;
	private int miles;
	private double price;

	/**
	 * Instantiates a new Automobile with the specified values.
	 *
	 * @precondition make != null && !make.isEmpty() && model != null &&
	 *               !model.isEmpty() && year >= 1885 && miles >=
	 *               MILES_LOWER_BOUND && price >= PRICE_LOWER_BOUND
	 * @postcondition getMake().equals(make) && getModel().equals(model) &&
	 *                getYear() == year && getMiles() == miles && getPrice() ==
	 *                price
	 * 
	 * @param make  the make
	 * @param model the model
	 * @param year  the year
	 * @param miles the miles
	 * @param price the price
	 */
	public Automobile(String make, String model, int year, int miles, double price) {
		if (make == null) {
			throw new IllegalArgumentException(UI.MAKE_CANNOT_BE_NULL);
		}

		if (make.isEmpty()) {
			throw new IllegalArgumentException(UI.MAKE_CANNOT_BE_EMPTY);
		}

		if (model == null) {
			throw new IllegalArgumentException(UI.MODEL_CANNOT_BE_NULL);
		}

		if (model.isEmpty()) {
			throw new IllegalArgumentException(UI.MODEL_CANNOT_BE_EMPTY);
		}

		if (year < LOWER_BOUND_YEAR) {
			throw new IllegalArgumentException(UI.INVALID_YEAR);
		}

		if (miles < MILES_LOWER_BOUND) {
			throw new IllegalArgumentException(UI.INVALID_MILES);
		}

		if (price < PRICE_LOWER_BOUND) {
			throw new IllegalArgumentException(UI.INVALID_PRICE);
		}

		this.make = make;
		this.model = model;
		this.year = year;
		this.miles = miles;
		this.price = price;
	}

	/**
	 * Get the miles.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the miles
	 */
	public int getMiles() {
		return this.miles;
	}

	/**
	 * Set the miles.
	 * 
	 * @precondition miles >= MILES_LOWER_BOUND
	 * @postcondition miles == getMiles()
	 *
	 * @param miles the miles to set
	 */
	public void setMiles(int miles) {
		if (miles < MILES_LOWER_BOUND) {
			throw new IllegalArgumentException(UI.INVALID_MILES);
		}

		this.miles = miles;
	}

	/**
	 * Gets the price.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price
	 */
	public double getPrice() {
		return this.price;
	}

	/**
	 * Sets the price.
	 *
	 * @precondition price >= PRICE_LOWER_BOUND
	 * @postcondition getPrice() == price
	 * 
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		if (price < PRICE_LOWER_BOUND) {
			throw new IllegalArgumentException(UI.INVALID_PRICE);
		}

		this.price = price;
	}

	/**
	 * Gets the make.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the make
	 */
	public String getMake() {
		return this.make;
	}

	/**
	 * Gets the model.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the model
	 */
	public String getModel() {
		return this.model;
	}

	/**
	 * Gets the year.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

}
