package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * 
 * This is the inventory class.
 * 
 * @author nicolebranum
 *
 */
public class Inventory {

	private String dealershipName;
	private ArrayList<Automobile> collection = new ArrayList<Automobile>();
	private int oldestIndex = 0; 
	private int newestIndex = 0; 

	/**
	 * Instantiates a new inventory with specified values.
	 * 
	 * @precondition dealershipName != null && ! dealershipName.isEmpty() &&
	 * 
	 * @postcondition getdealershipName().equals(dealershipName) &&
	 *                collection.size()==0
	 * 
	 * @param dealershipName - the name of the dealership
	 * @param collection - A collection of one or more automobile objects
	 */
	public Inventory(String dealershipName, ArrayList<Automobile> collection) {
		if (dealershipName == null) {
			throw new IllegalArgumentException(UI.DSHIPNAME_CANNOT_BE_NULL);
		}
		if (dealershipName.isEmpty()) {
			throw new IllegalArgumentException(UI.DSHIPNAME_CANNOT_BE_EMPTY);
		}
		this.dealershipName = dealershipName;
		this.collection = collection;
	}

	/**
	 * Instantiates a new inventory with specified dealershipName and creates a new
	 * collection of automobiles.
	 * 
	 * @precondition dealershipName != null && ! dealershipName.isEmpty()
	 * @postcondition getdealershipName().equals(dealershipName) && an
	 *                ArrayList<Automobile> is created
	 * 
	 * @param dealershipName the name of the dealership
	 */
	public Inventory(String dealershipName) {
		if (dealershipName == null) {
			throw new IllegalArgumentException(UI.DSHIPNAME_CANNOT_BE_NULL);
		}
		if (dealershipName.isEmpty()) {
			throw new IllegalArgumentException(UI.DSHIPNAME_CANNOT_BE_EMPTY);
		}
		this.dealershipName = dealershipName;
	}

	/**
	 * Gets the dealership name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the dealership name
	 */
	public String getDealershipName() {
		return this.dealershipName;
	}

	/**
	 * Set the dealership name.
	 * 
	 * @precondition dealershipName != null && !dealershipName.isEmpty()
	 * @postcondition dealershipName.equals(getDealershipName())
	 *
	 * @param dealershipName the name of the dealership to set
	 */
	public void setDealershipName(String dealershipName) {
		this.dealershipName = dealershipName;
	}

	/**
	 * Gets the collection of automobiles.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the automobiles in the collection at the specified dealership
	 */
	public ArrayList<Automobile> getCollection() {
		return this.collection;
	}

	/**
	 * Tells how many automobiles are in the collection at the dealership.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of automobiles in the collection
	 */
	public int numAutomobiles() {
		return this.collection.size();
	}

	/**
	 * Tells if an automobile was added to the inventory.
	 *
	 * @precondition dealershipName != null && ! dealershipName.isEmpty()
	 * @postcondition collection.size() != 0
	 * 
	 * @return true if the automobile was successfully added. false if the
	 *         automobile was not added.
	 *         
	 *  @param auto - the automobile object
	 */
	public boolean addAutomobile(Automobile auto) {
		if (this.dealershipName == null) {
			throw new IllegalArgumentException(UI.DSHIPNAME_CANNOT_BE_NULL);
		}
		if (this.dealershipName.isEmpty()) {
			throw new IllegalArgumentException(UI.DSHIPNAME_CANNOT_BE_EMPTY);
		}
		ArrayList<Automobile> cars = new ArrayList<Automobile>();
		cars.add(auto);
		if (cars.size() >= 1) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the year of the oldest automobile in the inventory.
	 *
	 * @precondition collection.size() !=0 && dealershipName != null && !
	 *               dealershipName.isEmpty()
	 * @postcondition none
	 * 
	 * @return year- year of the oldest automobile in the inventory
	 */
	public int getOldestYear() {
		if (this.collection.size() == 0) {
			throw new IllegalArgumentException(UI.COLLECTION_VALUE_INVALID);
		} else {
			ArrayList<Integer> carYear = new ArrayList<Integer>();
			for (Automobile auto : this.collection) {
				carYear.add(auto.getYear());
			}
			for (Integer j : carYear) {
				int year = j;
				if (year < carYear.get(this.oldestIndex)) {

					this.oldestIndex = carYear.indexOf(year);
				}
			}

			return carYear.get(this.oldestIndex);
		}

	}

	/**
	 * Returns the year of the newest automobile in the inventory.
	 *
	 * @precondition collection.size() !=0
	 * @postcondition none
	 * 
	 * @return year- year of the newest automobile in the inventory
	 */
	public int getNewestYear() {
		if (this.collection.size() == 0) {
			throw new IllegalArgumentException(UI.COLLECTION_VALUE_INVALID);
		} else {
			ArrayList<Integer> carYear = new ArrayList<Integer>();
			for (Automobile auto : this.collection) {
				carYear.add(auto.getYear());
			}
			for (Integer j : carYear) {
				int year = j;
				if (year > carYear.get(this.newestIndex)) {

					this.newestIndex = carYear.indexOf(year);
				}
			}

			return carYear.get(this.newestIndex);
		}

	}

	/**
	 * Returns the number of automobiles in a specified year range.
	 *
	 * @precondition collection.size() !=0
	 * @postcondition none
	 * 
	 * @return int- number of automobiles in specified range
	 * 
	 * @param lowerBound - the oldest year possible
	 * @param upperBound - the new year possible
	 */
	public int getNumInRange(int lowerBound, int upperBound) {
		if (this.collection.size() == 0) {
			throw new IllegalArgumentException(UI.COLLECTION_VALUE_INVALID);
		} else {
			ArrayList<Integer> carYears = new ArrayList<Integer>();
			for (Automobile k : this.collection) {
				carYears.add(k.getYear());
			}
			int counter = 0;
			for (Integer i : carYears) {
				if (i >= lowerBound && i <= upperBound) {
					counter++;
				}
			}
			return counter;
		}
	}

	/**
	 * Returns information about the inventory in a specific format (Dealership:
	 * name #Autos: size)
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return string- Dealership: name #Autos: size
	 */
	public String toString() {
		return ("Dealership: " + this.dealershipName + " #Autos: " + this.collection.size());
	}
}
